"use strict";

const ttn=require('ttn');
const ArgumentParser = require('argparse').ArgumentParser;
const chalk = require('chalk');
const figlet = require('figlet');

const parser = new ArgumentParser({
  version: '1.0.0',
  addHelp:true,
  description: 'The things network application'
});

const printError=function(msg){
  console.error(chalk.red(msg));
}

parser.addArgument('--appId',{
    help: "The Things Network Application Id",
    required:true,
});

parser.addArgument('--accessKey',{
    help: "The Things Network access Key",
    required:true,
});

let args = parser.parseArgs();


const appID = args.appId;
const accessKey = args.accessKey;

figlet('TTN: THE APP', function(err, data) {
    if (err) {
        // console.log('Something went wrong...');
        // console.dir(err);
        return;
    }
    console.log(chalk.blue(data));
    console.log(chalk.magenta("#########################################################################"))
    console.log(chalk.yellow("Application Id: "),appID)
    console.log(chalk.magenta("################ Waiting To recieve The things Data #####################"))

    // discover handler and open mqtt connection
    ttn.data(appID, accessKey)
      .then(function (client) {
        client.on("uplink", function (devID, payload) {
          console.log(chalk.green("Received uplink from "), chalk.magenta(devID))
          console.log(payload)
        })
      })
      .catch(function (err) {
        console.error(err)
        process.exit(1)
      })
});
