# The things Application
Retrieving Data from the things Network.

# Installation
First of all clone the application:

```
git clone https://ddesillas@bitbucket.org/lora_ssl_unipi_sec1718/ttn_app.git
```

And install the required packages:

```
npm install
```

Now you are ready to use!

# Usage
You can run the application like that:

```
 node ./index.js --appId ＾application_id＾ --accessKey ＾access_key＾
```

In ordet to get the `＾application_id＾` and the `＾access_key＾` then you should visit  and login into the https://console.thethingsnetwork.org. Then follow theese steps:

1. Then Go to `Applications`
2. Select your application.
3. Then you can find the value for `＾application_id＾` in the section `Application overview`:
   ![Application overview](./images/Επιλογή_021.png)
4. The `＾access_key＾` value is in section `Access Keys` and you can get it as the following image shows:
  ![Getting Access Keys Values](./images/Επιλογή_022.png)
